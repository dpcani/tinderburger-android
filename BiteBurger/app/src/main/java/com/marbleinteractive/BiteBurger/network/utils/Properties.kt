package com.marbleinteractive.BiteBurger.network.utils

object Properties {

    // Endpoints
    const val BASE_URL = "https://7uzmiokv.directus.app/"
    const val LOGIN_URL = "auth/login"
    const val LIST_URL = "items/Burguers"
}