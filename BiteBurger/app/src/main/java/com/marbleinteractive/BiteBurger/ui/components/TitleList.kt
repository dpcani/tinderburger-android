package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Preview
@Composable
fun TitleList() {

    val primaryColor = Color(0xFFADACB4)
    val secundaryColor = Color(0xFFFF6464)


    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.Bottom,
        horizontalArrangement = Arrangement.SpaceBetween
    ) { Text(
            text = "LA MAS TOP",
            fontFamily = CustomFont,
            fontSize = 16.sp,
            color = primaryColor,
            modifier = Modifier
                .padding(bottom = 16.dp, start = 8.dp))

        Text(
            text = "Ver Todos",
            fontFamily = CustomFont,
            textAlign = TextAlign.Right,
            fontSize = 12.sp,
            color = secundaryColor,
            modifier = Modifier
                .padding(bottom = 16.dp, end = 8.dp))
    }

}