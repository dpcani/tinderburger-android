package com.marbleinteractive.BiteBurger

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

// Entrada de inyeccion de dependencias
@HiltAndroidApp
class App: Application() {
    companion object{
        lateinit var prefs: Prefs
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
    }
}