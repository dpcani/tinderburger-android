package com.example.api.model.login.loginResponse

import com.google.gson.annotations.SerializedName

data class DataListResponseString(
    var id: String,
    var id_restaurant: String,
    var name: String,
    var description: String,
    var image: String,
    var price: String,
    var time: String,
    var size: String,
    var score: String,
    var logo: String,
)
