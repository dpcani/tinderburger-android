package com.marbleinteractive.BiteBurger.network.request

import com.google.gson.annotations.SerializedName

data class DataListRequest (
    @SerializedName("access_token") var accessToken: String,
    )

