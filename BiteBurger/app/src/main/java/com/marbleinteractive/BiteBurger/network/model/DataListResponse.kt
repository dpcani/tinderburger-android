package com.example.api.model.login.loginResponse

import com.google.gson.annotations.SerializedName

data class DataListResponse(
    @SerializedName("id") var id: Int,
    @SerializedName("id_restaurant") var id_restaurant: Int,
    @SerializedName("name") var name: String,
    @SerializedName("description") var description: String,
    @SerializedName("image") var image: String,
    @SerializedName("price") var price: Double,
    @SerializedName("preparation_time") var time: Int,
    @SerializedName("size") var size: String,
    @SerializedName("score") var score: Float,
    @SerializedName("logo_restaurant") var logo: String,
)
