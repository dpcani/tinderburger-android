package com.marbleinteractive.BiteBurger.ui.components

import android.transition.Visibility
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Composable

fun CustomTextField(
    captionString: String,
    textValue: String,
    onValueChange: (String) -> Unit,
    onClickButton: () -> Unit,
    onNext: (KeyboardActionScope.() -> Unit),
    condicion: Boolean,
) {

    /** Properties **/
    val primaryColor = Color(0xFF1A171D)
    val secundaryColor = Color(0xFFADACB4)

    /** Components **/
    Text(
        text = captionString,
        fontFamily = CustomFont,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 4.dp, start = 40.dp),
        textAlign = TextAlign.Start,
        color = secundaryColor
    )
    OutlinedTextField(
        modifier = Modifier
            .width(300.dp)
            .height(IntrinsicSize.Min)
            .background(Color.Transparent)
            .border(1.dp, secundaryColor, RoundedCornerShape(8.dp)),
        value = textValue,
        onValueChange = onValueChange,
        singleLine = true,
        trailingIcon = {
            IconButton(onClick = onClickButton) {
                Icon(
                    imageVector = Icons.Filled.Clear,
                    contentDescription = "Icon Clear",
                    tint = Color.White
                )

            }
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = Color.Gray
        ),
        textStyle = TextStyle(Color.White),
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Next
        ),
        keyboardActions = KeyboardActions(onNext = onNext),
    )
    if (condicion) {
        Text(
            text = "Olvidastes tu contraseña?",
            fontFamily = CustomFont,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 4.dp, end = 40.dp),
            textAlign = TextAlign.End,
            color = secundaryColor
        )
    }
}
