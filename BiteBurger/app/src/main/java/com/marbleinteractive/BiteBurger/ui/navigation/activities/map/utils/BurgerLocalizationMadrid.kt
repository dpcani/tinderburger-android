package com.marbleinteractive.BiteBurger.ui.navigation.activities.map.utils

import com.google.android.gms.maps.model.LatLng

sealed class BurgerLocalizationMadrid (val localization: LatLng, val site: String) {
    object LBurger1: BurgerLocalizationMadrid(localization = LatLng(40.415136, -3.698513),site = "Goiko Grill")
    object LBurger2: BurgerLocalizationMadrid(localization = LatLng(40.419955, -3.700487),site = "TGB Burger")
    object LBurger3: BurgerLocalizationMadrid(localization = LatLng(40.420337, -3.705546),site = "Five Guys")
    object LBurger4: BurgerLocalizationMadrid(localization = LatLng(40.426078, -3.695752),site = "Burger Porn")
    object LBurger5: BurgerLocalizationMadrid(localization = LatLng(40.402275, -3.700575),site = "Goiko Grill")
    object LBurger6: BurgerLocalizationMadrid(localization = LatLng(40.412374, -3.711915),site = "Viva Burger")

}

sealed class BurgerLocalizationBarcelona (val localization: LatLng, val site: String) {
    object LBurger1: BurgerLocalizationBarcelona(localization = LatLng(41.395124, 2.166467),site = "La Real")
    object LBurger2: BurgerLocalizationBarcelona(localization = LatLng(41.389501, 2.159184),site = "Oval")
    object LBurger3: BurgerLocalizationBarcelona(localization = LatLng(41.385852, 2.169432),site = "Five Guys")
    object LBurger4: BurgerLocalizationBarcelona(localization = LatLng(41.386860, 2.167165),site = "Bacoa")
    object LBurger5: BurgerLocalizationBarcelona(localization = LatLng(41.386545, 2.174968),site = "La Central")
    object LBurger6: BurgerLocalizationBarcelona(localization = LatLng(41.385401, 2.178895),site = "Little Bacoa")

}

sealed class BurgerLocalizationBilbao (val localization: LatLng, val site: String) {
    object LBurger1: BurgerLocalizationBilbao(localization = LatLng(43.264601, -2.942052),site = "Goiko Grill")
    object LBurger2: BurgerLocalizationBilbao(localization = LatLng(43.264471, -2.943134),site = "Baka Vieja")
    object LBurger3: BurgerLocalizationBilbao(localization = LatLng(43.262074, -2.940876),site = "El Dinamico")
    object LBurger4: BurgerLocalizationBilbao(localization = LatLng(43.260112, -2.933310),site = "La Brasa Canalla")
    object LBurger5: BurgerLocalizationBilbao(localization = LatLng(43.257946, -2.935577),site = "Goiko Grill")
    object LBurger6: BurgerLocalizationBilbao(localization = LatLng(43.261479, -2.926852),site = "Viva Burger")

}


