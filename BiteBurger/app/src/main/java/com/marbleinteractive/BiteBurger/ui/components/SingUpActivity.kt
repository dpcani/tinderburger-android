package com.marbleinteractive.BiteBurger.ui.singUp

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxColors
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marbleinteractive.BiteBurger.ui.components.CustomTextField
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Preview
@Composable
fun SingUpActivity() {

    /** Properties **/
    val primaryColor = Color(0xFFADACB4)
    var state by remember { mutableStateOf(false) }

    /** Components **/
    Row(
        modifier = Modifier
            .width(300.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        val isChecked = remember { mutableStateOf(false) }
        Checkbox(
            // Cambiar color del check
            colors = CheckboxDefaults.colors(
                checkedColor = primaryColor,
                uncheckedColor = primaryColor
            ),
            checked = isChecked.value,
            onCheckedChange = {
                isChecked.value = it
            })
        Spacer(modifier = Modifier.padding(4.dp))
        Text(text = "Acepto las condiciones legales",
            fontFamily = CustomFont,
            fontSize = 10.sp,
            color = primaryColor)

    }
}