package com.marbleinteractive.BiteBurger.ui.navigation.activities

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.marbleinteractive.BiteBurger.ui.components.Circle
import com.marbleinteractive.BiteBurger.ui.components.MiniCustomCellCard
import com.marbleinteractive.BiteBurger.ui.components.TitleList
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.homeActivity.HeaderList
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.viewModel.HomeViewModel

@Preview
@Composable
fun Match(
    token: String = "",
    homeViewModel: HomeViewModel = viewModel()
) {
    homeViewModel.listBurgers(tokenRequest = token)
    val listado by homeViewModel.listData.observeAsState()
    val randomElement = listado?.random()

    /** Properties **/
    val primaryColor = Color(0xFF7C7345)
    val secundaryColor = Color(0xFF2D2931)


    val gradient45 = Brush.linearGradient(
        colors = listOf(secundaryColor, secundaryColor, primaryColor),
        start = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY),
        end = Offset(0f, 0f),
        tileMode = TileMode.Clamp
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxSize()
            .background(
                gradient45
            )
    )
    {}
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Column() {
            Spacer(modifier = Modifier.padding(vertical = 8.dp))
            HeaderList()
        }
        Spacer(modifier = Modifier.size(16.dp))
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,

            ) {
            listado?.random()?.let {
                Circle(
                    sizeDraw = 350.dp,
                    elementos = it
                )
            }

            Spacer(modifier = Modifier.size(16.dp))
            TitleList()
            LazyRow(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier
                    .fillMaxHeight(0.95f)
                    .weight(2f)
                    .padding(start = 8.dp, end = 8.dp)
            ) {
                listado?.let { elementos ->
                    items(elementos.count()) {
                        MiniCustomCellCard(elementos = elementos[it])
                        Spacer(modifier = Modifier.padding(horizontal = 4.dp))
                    }

                }
            }
        }
    }
}