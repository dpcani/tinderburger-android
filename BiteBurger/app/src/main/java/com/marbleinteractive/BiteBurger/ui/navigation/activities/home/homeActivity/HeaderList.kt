package com.marbleinteractive.BiteBurger.ui.navigation.activities.home.homeActivity

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marbleinteractive.BiteBurger.R
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Preview
@Composable
fun HeaderList() {
    /** Properies **/
    val primaryColor = Color(0xFFADACB4)

    /** Components **/
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.Bottom,
        horizontalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_localize),
            contentDescription = "mapa",
            modifier = Modifier
                .width(40.dp)
                .height(40.dp)
                .padding(start = 8.dp, top = 8.dp)
        )
        Spacer(modifier = Modifier.padding(4.dp))
        Text(
            text = "Madrid",
            fontFamily = CustomFont,
            fontSize = 10.sp,
            color = primaryColor,
            modifier = Modifier
            .padding(bottom = 4.dp))
        Spacer(modifier = Modifier.padding(20.dp))
        Text(
            text = "Hola David.\r\n!Hoy te vamos a enamorar!",
            fontFamily = CustomFont,
            textAlign = TextAlign.Right,
            fontSize = 10.sp,
            color = primaryColor,
            modifier = Modifier
                .width(200.dp)
                .padding(bottom = 4.dp))
        Spacer(modifier = Modifier.padding(4.dp))
        Image(
            painter = painterResource(id = R.drawable.ic_perfil),
            contentDescription = "imagen perfil",
            modifier = Modifier
            .width(40.dp)
            .height(40.dp)
            .padding(end = 8.dp, top = 8.dp))
    }

}