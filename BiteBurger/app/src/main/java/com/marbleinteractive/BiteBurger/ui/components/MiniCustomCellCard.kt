package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.api.model.login.loginResponse.DataListResponse
import com.marbleinteractive.BiteBurger.R
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Preview
@Composable
fun MiniCustomCellCardPreview() {
    MiniCustomCellCard(
        elementos = DataListResponse(
            id = 1,
            id_restaurant = 1,
            name = "ChessBurger2341234324",
            description = "",
            image = "asdas",
            price = 15.0,
            time = 15,
            size = "xl",
            score = 8f,
            logo = ""
        )
    )
}


@Composable
fun MiniCustomCellCard(
    width: Int = 260,
    height: Int = 100,
    elementos: DataListResponse
) {

    val backGroundColor = Color(0xFF27232B)

    Card(
        modifier = Modifier
            .width(width.dp)
            .height(height.dp),
        shape = RoundedCornerShape(10.dp),
        backgroundColor = Color.Black,
    ) {
        Card(
            modifier = Modifier
                .width(width.dp)
                .height(height.dp),
            shape = RoundedCornerShape(10.dp),
            backgroundColor = backGroundColor,
        ) {

            Row(
                verticalAlignment = Alignment.Bottom,
                modifier = Modifier.padding(bottom = 8.dp)
            ) {
                Column(
                    horizontalAlignment = Alignment.Start
                ) {
                    // Hay que añadir https://7uzmiokv.directus.app/assets/ con lo que devuelve
                    Image(
                        painter = rememberAsyncImagePainter(model = "https://7uzmiokv.directus.app/assets/${elementos.image}" ),
                        contentDescription = "",
                        modifier = Modifier
                            .width(85.dp)
                            .height(85.dp)
                            .padding(start = 8.dp))
                }
                Column(
                    horizontalAlignment = Alignment.Start,
                ) {
                    Spacer(modifier = Modifier.padding(4.dp))
                    Text(modifier = Modifier
                        .padding(start = 8.dp),
                        text = elementos.name,
                        fontFamily = CustomFont,
                        color = Color.White)
                    Spacer(modifier = Modifier.padding(4.dp))
                    RatingBar(
                        rating = elementos.score,
                        modifier = Modifier
                            .height(20.dp)
                            .padding(start = 8.dp, top = 4.dp)

                    )
                    Spacer(modifier = Modifier.padding(8.dp))
                    Row(
                        verticalAlignment = Alignment.CenterVertically                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_likesburger),
                            contentDescription = "Likes Burger",
                            modifier = Modifier
                                .padding(start = 8.dp)
                                .size(20.dp, 20.dp))
                        Spacer(modifier = Modifier.padding(4.dp))
                        Text(
                            text = "123",
                            fontSize = 12.sp,
                            fontFamily = CustomFont,
                            color = Color.White)
                    }

                }
                Column(
                    horizontalAlignment = Alignment.Start,
                    verticalArrangement = Arrangement.Bottom,
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .fillMaxWidth()
                ) {
                    Row(
                        verticalAlignment = Alignment.Bottom,
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_localize),
                            contentDescription = "mapa",
                            modifier = Modifier
                                .width(20.dp)
                                .height(20.dp)
                        )
                        Spacer(modifier = Modifier.padding(4.dp))
                        Text(
                            text = "400m",
                            fontFamily = CustomFont,
                            fontSize = 10.sp,
                            color = Color.White,
                            modifier = Modifier
                                .width(100.dp)
                        )
                    }
                }
            }
        }
    }
}