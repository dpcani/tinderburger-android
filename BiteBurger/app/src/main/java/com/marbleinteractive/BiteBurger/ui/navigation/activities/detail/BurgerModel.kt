package com.marbleinteractive.BiteBurger.ui.navigation.activities.detail


import com.example.api.model.login.loginResponse.DataListResponseString

fun BurgerModel(cadenaIn: String): List<DataListResponseString> {

    val idBurger = cadenaIn.substringAfter("id=").substringBefore(",")
    val idRestaurant = cadenaIn.substringAfter("id_restaurant=").substringBefore(",")
    val nameBurger = cadenaIn.substringAfter("name=").substringBefore(",")
    val description = cadenaIn.substringAfter("description=").substringBefore(",")
    val image = cadenaIn.substringAfter("image=").substringBefore(",")
    val price = cadenaIn.substringAfter("price=").substringBefore(",")
    val time = cadenaIn.substringAfter("time=").substringBefore(",")
    val sizeBurger = cadenaIn.substringAfter("size=").substringBefore(",")
    val score = cadenaIn.substringAfter("score=").substringBefore(",")
    val logo = cadenaIn.substringAfter("logo=").substringBefore(",")

    return listOf(
        DataListResponseString(
            id = idBurger,
            id_restaurant = idRestaurant,
            name = nameBurger,
            description = description,
            image = image,
            price = price,
            time = time,
            size = sizeBurger,
            score = score,
            logo = logo
        )
    )

}