package com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.marbleinteractive.BiteBurger.R
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Preview
@Composable
fun CardBurger(
    logo: String = ""
) {

    val primaryColor = Color(0xFF27232B)
    val buttonColor = Color(0xFF505050)
    val button2Color = Color(0xFFFF6464)

    Card(
        modifier = Modifier
            .height(155.dp)
            .width(325.dp)
            .clip(shape = RoundedCornerShape(8.dp)),
        backgroundColor = primaryColor
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Start
            ) {
                Column(
                    modifier = Modifier
                        .padding(start = 8.dp, top = 8.dp)
                )
                {
                    Image(
                        painter = rememberAsyncImagePainter(model = "https://7uzmiokv.directus.app/assets/${logo}"),
                        contentDescription = "Logo",
                        modifier = Modifier
                            .size(50.dp),
                        contentScale = ContentScale.Fit
                    )
                }
                Column(
                    modifier = Modifier
                        .padding(start = 8.dp, top = 8.dp)
                )
                {
                    Text(
                        text = "Goiko Burger",
                        fontSize = 16.sp,
                        fontFamily = CustomFont,
                        color = Color.White
                    )
                    Text(
                        text = "C/ Del prado 15",
                        fontSize = 10.sp,
                        fontFamily = CustomFont,
                        color = Color.Gray
                    )
                    Text(
                        text = "L-V: 13:00 - 16:00 / 20:00 - 23:00",
                        fontSize = 10.sp,
                        fontFamily = CustomFont,
                        color = Color.Gray
                    )

                    Text(
                        text = "S-D: 13:00 - 16:00 / 20:00 - 24:00",
                        fontSize = 10.sp,
                        fontFamily = CustomFont,
                        color = Color.Gray
                    )
                }
            }
            Spacer(modifier = Modifier.size(8.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.Bottom,
                    modifier = Modifier
                        .padding(start = 75.dp, top = 8.dp)
                )
                {
                    Column(
                        verticalArrangement = Arrangement.Bottom,
                        modifier = Modifier
                            .height(50.dp)
                    )
                    {
                        Image(
                            painter = painterResource(id = R.drawable.ic_likesburger),
                            contentDescription = "Likes Burger",
                            modifier = Modifier
                                .size(25.dp, 25.dp)
                                .padding(start = 8.dp)
                                .offset(y = 2.dp),
                        )
                        Text(
                            text = "123 Bites",
                            fontSize = 8.sp,
                            fontFamily = CustomFont,
                            color = Color.Gray
                        )
                    }
                    Spacer(modifier = Modifier.padding(start = 8.dp))
                    Column(
                        verticalArrangement = Arrangement.Bottom,
                        modifier = Modifier
                            .height(50.dp)
                    )
                    {
                        Image(
                            painter = painterResource(id = R.drawable.ic_localize),
                            contentDescription = "mapa",
                            modifier = Modifier
                                .width(20.dp)
                                .height(20.dp)
                        )
                        Text(
                            text = "400m",
                            fontFamily = CustomFont,
                            fontSize = 8.sp,
                            color = Color.Gray,
                            modifier = Modifier
                                .width(100.dp)
                        )
                    }
                }
                Column(
                    modifier = Modifier
                        .padding(end = 8.dp)
                )
                {
                    Button(
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(8.dp))
                            .height(28.dp)
                            .width(80.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = buttonColor,
                            contentColor = Color(0xFFFFFFFF)
                        ),
                        onClick = {},
                    ) {
                        Text(
                            fontSize = 8.sp,
                            fontFamily = CustomFont,
                            textAlign = TextAlign.Center,
                            text = "Contactar",
                            modifier = Modifier
                                .width(72.dp)
                        )
                    }
                    Spacer(modifier = Modifier.size(8.dp))
                    Button(
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(8.dp))
                            .height(28.dp)
                            .width(80.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = button2Color,
                            contentColor = Color(0xFFFFFFFF)
                        ),
                        onClick = {},
                    ) {
                        Text(
                            fontSize = 8.sp,
                            fontFamily = CustomFont,
                            textAlign = TextAlign.Center,
                            text = "!Quiero ir!",
                            modifier = Modifier
                                .width(72.dp)
                        )
                    }
                }
            }
        }

    }
}