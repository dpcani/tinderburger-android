package com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.DetailBurger
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Preview
@Composable
fun DetailBurgerRowPreview() {
    DetailBurgerRow(
        score = "4.3",
        time = "20",
        price = "15.0"
    )
}

@Composable
fun DetailBurgerRow(
    score: String,
    time: String,
    price: String
) {

    /** Properties **/
    val shapeCircle = CircleShape
    val colorGreen = Color(0xFF98C139)
    val colorYellow = Color(0xFFFFC108)
    val colorRed = Color(0xFFFF6464)

    var colorFinal = Color.White
    var scoreFinal: String

    val rowHeight = 75.dp
    val rowWidth = 200.dp
    val circleWidth = 50.dp
    val circleHeight = 75.dp


    /** Components **/
    Row(
        modifier = Modifier
            .height(rowHeight)
            .width(rowWidth)
            .offset(y = -20.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        /** Score Circle**/
        Column(
            modifier = Modifier
                .height(circleHeight)
                .width(circleWidth),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            scoreFinal = ((score.toFloat() * 2.0).toString())
            if (scoreFinal.toFloat() <= 5.0) {
                colorFinal = colorRed
            } else if (scoreFinal.toFloat() > 5.0 && scoreFinal.toFloat() < 8) {
                colorFinal = colorYellow
            } else if (scoreFinal.toFloat() >= 8) {
                colorFinal = colorGreen
            }

            Box(
                modifier = Modifier
                    .clip(shape = shapeCircle)
                    .background(color = Color.Transparent)
                    .size(50.dp)
                    .border(BorderStroke(width = 3.dp, color = colorFinal), shape = shapeCircle),
            ) {
                Text(
                    text = scoreFinal,
                    fontSize = 18.sp,
                    fontFamily = CustomFont,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(Alignment.Center),
                    color = colorFinal
                )
            }

            Text(
                text = "Puntos",
                fontSize = 12.sp,
                fontFamily = CustomFont,
                textAlign = TextAlign.Center,
                color = colorFinal
            )
        }
        Spacer(modifier = Modifier.size(8.dp))

        /** Time Circle**/
        Column(
            modifier = Modifier
                .height(circleHeight)
                .width(circleWidth),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (time.toInt() < 15) {
                colorFinal = colorGreen
            } else if (time.toInt() >= 15 && time.toInt() < 18) {
                colorFinal = colorYellow
            } else if (time.toInt() > 18) {
                colorFinal = colorRed
            }
            Box(
                modifier = Modifier
                    .clip(shape = shapeCircle)
                    .background(color = Color.Transparent)
                    .size(50.dp)
                    .border(BorderStroke(width = 3.dp, color = colorFinal), shape = shapeCircle),
            ) {
                Text(
                    text = "$time'",
                    fontSize = 18.sp,
                    fontFamily = CustomFont,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(Alignment.Center),
                    color = colorFinal
                )
            }

            Text(
                text = "Tiempo",
                fontSize = 12.sp,
                fontFamily = CustomFont,
                textAlign = TextAlign.Center,
                color = colorFinal
            )
        }
        Spacer(modifier = Modifier.size(8.dp))

        /** Price Circle**/
        Column(
            modifier = Modifier
                .height(circleHeight)
                .width(circleWidth),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (price.toFloat() < 10.0) {
                colorFinal = colorGreen
            } else if (price.toFloat() > 10.0 && price.toFloat() <= 15.0) {
                colorFinal = colorYellow
            } else if (price.toFloat() > 15.0) {
                colorFinal = colorRed
            }
            Box(
                modifier = Modifier
                    .clip(shape = shapeCircle)
                    .background(color = Color.Transparent)
                    .size(50.dp)
                    .border(BorderStroke(width = 3.dp, color = colorFinal), shape = shapeCircle),
            ) {
                Text(
                    text = "${price}€",
                    fontSize = 16.sp,
                    fontFamily = CustomFont,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .align(Alignment.Center),
                    color = colorFinal
                )
            }

            Text(
                text = "Precio",
                fontSize = 12.sp,
                fontFamily = CustomFont,
                textAlign = TextAlign.Center,
                color = colorFinal
            )
        }


    }

}