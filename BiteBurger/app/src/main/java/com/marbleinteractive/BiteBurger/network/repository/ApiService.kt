package com.marbleinteractive.BiteBurger.network.repository


import com.example.api.model.login.loginResponse.DataListResponse
import com.example.api.model.login.loginResponse.ListResponse
import com.example.api.model.login.loginResponse.LoginResponse
import com.marbleinteractive.BiteBurger.network.request.DataListRequest
import com.marbleinteractive.BiteBurger.network.request.LoginRequest
import com.marbleinteractive.BiteBurger.network.utils.Properties
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    // Interface para definir los RESTS
    @Headers("Content-Type: application/json")
    @POST(Properties.LOGIN_URL)
    fun login(@Body request: LoginRequest): Call<LoginResponse>

    @GET(Properties.LIST_URL)
    fun getListBurgers(@Header("Authorization") token: String): Call<ListResponse>

}