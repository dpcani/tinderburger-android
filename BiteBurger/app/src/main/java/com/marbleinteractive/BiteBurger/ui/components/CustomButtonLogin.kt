package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Composable
fun CustomButtonLogin(
    stringButton:String,
    onClick: () -> Unit,
    enable: Boolean)
{
    Button(
        onClick = onClick,
        enabled = enable,
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .width(300.dp)
            .height(50.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color(0xFFFF6464),
            contentColor = Color(0xFFFFFFFF)
        )

    ) {
        Text(text = stringButton, fontFamily = CustomFont)
    }
}