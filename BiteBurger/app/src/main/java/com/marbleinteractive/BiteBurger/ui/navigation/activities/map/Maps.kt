package com.marbleinteractive.BiteBurger.ui.navigation.activities

import android.content.Context
import android.util.Log
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.tooling.preview.Preview
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.*
import com.marbleinteractive.BiteBurger.R
import com.marbleinteractive.BiteBurger.ui.navigation.activities.map.utils.BurgerLocalizationMadrid

@Preview
@Composable
fun Maps() {

    /** Properties **/
    val madrid = LatLng(40.416759, -3.703449)
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(madrid, 14f)
    }
    val properties by remember {
        mutableStateOf( MapProperties(mapType = MapType.NORMAL))
    }

    /** Components **/
    GoogleMap(
        modifier = Modifier.fillMaxSize(),
        cameraPositionState = cameraPositionState,
        properties = properties
    ) {
        Marker(
            position = BurgerLocalizationMadrid.LBurger1.localization,
            title = BurgerLocalizationMadrid.LBurger1.site,
            snippet = "Kevin Kostner",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
        Marker(
            position = BurgerLocalizationMadrid.LBurger2.localization,
            title = BurgerLocalizationMadrid.LBurger2.site,
            snippet = "La rubita",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
        Marker(
            position = BurgerLocalizationMadrid.LBurger3.localization,
            title = BurgerLocalizationMadrid.LBurger3.site,
            snippet = "Buff",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
        Marker(
            position = BurgerLocalizationMadrid.LBurger4.localization,
            title = BurgerLocalizationMadrid.LBurger4.site,
            snippet = "Buff",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
        Marker(
            position = BurgerLocalizationMadrid.LBurger5.localization,
            title = BurgerLocalizationMadrid.LBurger5.site,
            snippet = "Buff",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
        Marker(
            position = BurgerLocalizationMadrid.LBurger6.localization,
            title = BurgerLocalizationMadrid.LBurger6.site,
            snippet = "Buff",
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin),
        )
    }
}