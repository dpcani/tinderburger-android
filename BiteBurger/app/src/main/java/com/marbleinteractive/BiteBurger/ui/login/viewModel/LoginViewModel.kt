package com.marbleinteractive.BiteBurger.ui.login.viewModel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.api.model.login.loginResponse.LoginResponse
import com.marbleinteractive.BiteBurger.App.Companion.prefs
import com.marbleinteractive.BiteBurger.network.repository.RetrofitHelper
import com.marbleinteractive.BiteBurger.network.request.LoginRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {

    val isSuccessLoading = mutableStateOf(value = false)
    val progressBar = mutableStateOf(value = false)
    val errorCheck = mutableStateOf(value = false)
    var token = mutableStateOf("")

    private val loginRequestLiveData = MutableLiveData<Boolean>()

    fun login(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            errorCheck.value = false
            progressBar.value = true
            val authService = RetrofitHelper.getAuthService()
            authService.login(LoginRequest(email = email, password = password))
                .enqueue(object : Callback<LoginResponse> {
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Log.d("Loggin", "Fallo Response")
                    }
                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>,
                    ) {
                        Log.d("Logging", "Acierto Response.. Comprobando..")
                        if (response.isSuccessful) {
                            isSuccessLoading.value = true
                            response.body()?.let { tokeDto ->
                                Log.d("Logging", "Respuesta Response Token: ${tokeDto.loginData.acessToken}")
                                Log.d("Logging", "Respuesta Response expires: ${tokeDto.loginData.expires}")
                                Log.d("Logging", "Respuesta Response RefreshToken: ${tokeDto.loginData.acessToken}")
                                prefs.saveToken(tokeDto.loginData.acessToken)
                            }
                        } else {
                            isSuccessLoading.value = false
                            progressBar.value = false
                            errorCheck.value = true
                            response.errorBody()?.let { error ->
                                error.close()
                                Log.d("Logging", "Respuesta Response Error: ${error}")
                            }

                        }

                    }
                })
        }
    }
}

