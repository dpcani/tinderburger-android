package com.marbleinteractive.BiteBurger.ui.navigation.activities.detail

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.marbleinteractive.BiteBurger.ui.components.MiniCustomCellCard
import com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail.CardBurger
import com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail.DetailBurgerRow
import com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail.HeaderDetail
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.viewModel.HomeViewModel
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Preview
@Composable
fun DetailBurgerPreview() {
    DetailBurger(
        string = "id=4, id_restaurant=1, name=Pecado Carnal, description=Lo mejor de la pizza metido entre dos panes, image=eb53f326-ece8-4666-bdfc-391bcdd9c682, price=12.0, time=13, size=XL, score=4.0, logo=623f0763-7343-417d-83fc-d337bbbc3858)"
    )
}

@Composable
fun DetailBurger(
    string: String? = "",
    token: String = "",
    homeViewModel: HomeViewModel = viewModel(),
) {
    /** Extraccion Cadena **/
    var listado = string?.let {
        BurgerModel(it)
    }

    /** ViewModel **/
    homeViewModel.listBurgers(tokenRequest = token)
    val listadoBurgers by homeViewModel.listData.observeAsState()

    /** Properties **/
    val primaryColor = Color(0xFF7C7345)
    val secundaryColor = Color(0xFF2D2931)

    val gradient45 = Brush.linearGradient(
        colors = listOf(secundaryColor, secundaryColor, primaryColor),
        start = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY),
        end = Offset(0f, 0f),
        tileMode = TileMode.Clamp
    )

    Scaffold(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        backgroundColor = Color.Transparent
    )
    {
            if (listado != null) {
                listado.map { elemento ->
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(gradient45)
                            .padding(top = 8.dp)
                            .verticalScroll(rememberScrollState()),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = CenterHorizontally
                    ) {

                        /** Header **/
                        /** Header **/
                        HeaderDetail(
                            name = elemento.name
                        )
                        /** Hamburguesa **/
                        /** Hamburguesa **/
                        Image(
                            painter = rememberAsyncImagePainter(model = "https://7uzmiokv.directus.app/assets/${elemento.image}"),
                            contentDescription = "Burger",
                            modifier = Modifier
                                .fillMaxWidth(0.9f)
                                .height(200.dp),
                            contentScale = ContentScale.Inside

                        )

                        /** Puntuación **/

                        /** Puntuación **/
                        DetailBurgerRow(
                            score = elemento.score,
                            time = elemento.time,
                            price = elemento.price
                        )

                        /** Texto Burger **/

                        /** Texto Burger **/
                        Text(
                            modifier = Modifier
                                .padding(horizontal = 60.dp)
                                .align(CenterHorizontally),
                            text = elemento.description.uppercase(),
                            fontFamily = CustomFont,
                            color = Color.Gray,
                            maxLines = 2,
                            textAlign = TextAlign.Center,
                            fontSize = 14.sp
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(
                                modifier = Modifier
                                    .align(CenterVertically),
                                text = "size:",
                                fontFamily = CustomFont,
                                color = Color.Gray,
                                fontSize = 12.sp
                            )
                            Text(
                                text = elemento.size,
                                fontFamily = CustomFont,
                                color = Color.Gray,
                                fontSize = 30.sp,
                                fontWeight = FontWeight.ExtraBold
                            )
                        }
                        Spacer(modifier = Modifier.size(8.dp))
                        CardBurger(
                            logo = elemento.logo
                        )
                        Spacer(modifier = Modifier.size(8.dp))
                        Row(
                            horizontalArrangement = Arrangement.Start
                        ) {
                            Text(
                                text = "OTRAS BURGERS",
                                fontFamily = CustomFont,
                                fontSize = 16.sp,
                                color = primaryColor,
                                modifier = Modifier
                                    .offset(x = -100.dp),
                            )
                        }
                        Spacer(modifier = Modifier.size(8.dp))
                        LazyRow(
                            horizontalArrangement = Arrangement.Center,
                            modifier = Modifier
                                .fillMaxHeight(0.95f)
                                .padding(start = 16.dp)
                        ) {
                            listadoBurgers?.let { elementos ->
                                items(elementos.count()) {
                                    MiniCustomCellCard(elementos = elementos[it])
                                    Spacer(modifier = Modifier.padding(horizontal = 4.dp))
                                }

                            }
                        }
                        Spacer(modifier = Modifier.size(80.dp))

                    }
                }
            } else {
                Text(
                    text = "Sin valores".uppercase(),
                    fontSize = 20.sp,
                    fontFamily = CustomFont,
                )
            }
        Log.d("Listado Final", "$string")
    }

}