package com.marbleinteractive.BiteBurger.ui.navigation

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.marbleinteractive.BiteBurger.App.Companion.prefs
import com.marbleinteractive.BiteBurger.ui.login.LoginScreen
import com.marbleinteractive.BiteBurger.ui.login.viewModel.LoginViewModel
import com.marbleinteractive.BiteBurger.ui.navigation.navigationBar.BottomNavigation

@Composable
fun NavigationScreen(viewModel: LoginViewModel) {

    val navController = rememberNavController()
    val loadingProgressBar = viewModel.progressBar.value
    val token = prefs.getToken().toString();

    NavHost(
        navController = navController,
        startDestination = Destination.getStartDestination()
    ) {
        composable(route = Destination.Login.route) {
            if (viewModel.isSuccessLoading.value) {
                LaunchedEffect(key1 = Unit) {
                    navController.navigate(route = Destination.Home.route) {
                        popUpTo(route = Destination.Login.route) {
                            inclusive = true
                        }
                    }
                }
            } else {
                LoginScreen(
                    loadingProgressBar = loadingProgressBar,
                    onClickLogin = viewModel::login,
                    errorCheck = viewModel.errorCheck.value
                )
            }
        }

        composable(route = Destination.Home.route) {
            if (token != null) {
                BottomNavigation(
                    token = token,
                )
            }
        }
    }
}