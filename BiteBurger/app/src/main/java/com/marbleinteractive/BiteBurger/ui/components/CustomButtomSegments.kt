package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.*

import androidx.compose.ui.*
import androidx.compose.ui.Alignment.Companion.CenterHorizontally

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign

import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Preview
@Composable
fun CustomButtomSegments(): Boolean {

    /** Properties **/
    var index = remember { mutableStateOf(0) }
    var changeViewLogin by remember{ mutableStateOf(true)}
    var changeViewRegister by remember{ mutableStateOf(true)}
    var stateScreen by remember { mutableStateOf(false)}

    val _width = 120.dp
    val _height = 60.dp
    val _backgrounTransparent = Color.Transparent
    val _padding = 1.dp
    val _spacerPadingFirst = 13.dp
    val _spacerPading2 = 35.dp
    val secundaryColor = Color(0xFFADACB4)
    val buttonColor = Color(0xFFFF6464)



    /** Components **/
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(42.dp)
            .height(_height),
        horizontalArrangement = Arrangement.Center,
    ) {
        /** First "Button" **/
        Column(
            modifier = Modifier
                .width(_width)
                .background(_backgrounTransparent)
                .weight(1f),
            verticalArrangement = Arrangement.Center,)
        {
            Text(
                modifier = Modifier
                    .width(_width)
                    .background(_backgrounTransparent)
                    .clickable(enabled = changeViewLogin) {
                        index.value = 0
                        //changeViewLogin = !changeViewLogin
                        println("ChangeValueLogin: $changeViewLogin")
                        stateScreen = false
                    },
                color = secundaryColor,
                fontFamily = CustomFont,
                text = "Login", fontSize = 18.sp, textAlign = TextAlign.Center)
            Spacer(modifier = Modifier.padding(_spacerPadingFirst))
            Divider(modifier = Modifier
                .width(_width)
                .align(alignment = CenterHorizontally)
                .padding(_padding),
                color = if (index.value == 0) buttonColor else _backgrounTransparent)
        }
        Spacer(modifier = Modifier.padding(_spacerPading2))

        /** Second "Button" **/
        Column(
            modifier = Modifier
                .width(_width)
                .background(_backgrounTransparent)
                .weight(1f),
            verticalArrangement = Arrangement.Center,)
        {
            Text(
                modifier = Modifier
                    .width(_width)
                    .background(_backgrounTransparent)
                    .align(alignment = CenterHorizontally)
                    .clickable(enabled = changeViewRegister) {
                        index.value = 1
                        //changeViewRegister = !changeViewRegister
                        println("ChangeValueRegister: $changeViewLogin")
                        stateScreen = true
                    },
                color = secundaryColor,
                fontFamily = CustomFont,
                text = "Crear Cuenta", fontSize = 18.sp, textAlign = TextAlign.Center)
            Spacer(modifier = Modifier.padding(_spacerPadingFirst))
            Divider(modifier = Modifier
                .width(_width)
                .align(alignment = CenterHorizontally)
                .padding(_padding),
                color  = if (index.value == 1) buttonColor else _backgrounTransparent)
        }
    }
    return stateScreen
}
