package com.example.api.model.login.loginResponse

import com.google.gson.annotations.SerializedName

data class LoginDataResponse(
    @SerializedName("access_token") var acessToken: String,
    @SerializedName("expires") var expires: Int,
    @SerializedName("refresh_token") var refreshToken: String
)
