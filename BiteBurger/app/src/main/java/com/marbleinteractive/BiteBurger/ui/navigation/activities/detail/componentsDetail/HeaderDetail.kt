package com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.componentsDetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont



@Preview
@Composable
fun HeaderDetailPreview(){
    HeaderDetail(name = "Prueba")
}



@Composable
fun HeaderDetail(
    name: String,

) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Center

    ) {
        Text(
            text = name.uppercase(),
            fontSize = 28.sp,
            fontFamily = CustomFont,
            color = Color.White,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.size(16.dp))

    }
}