package com.marbleinteractive.BiteBurger.ui.utils.navigation

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.marbleinteractive.BiteBurger.ui.navigation.activities.*
import com.marbleinteractive.BiteBurger.ui.navigation.activities.detail.DetailBurger
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.viewModel.HomeViewModel

@Composable
fun BottomNavHost(
    navHostController: NavHostController,
    token: String
) {
    NavHost(
        navController = navHostController,
        startDestination = Screen.Home.route
    )
    {
        composable(route = Screen.Home.route) {
            val homeViewModel = hiltViewModel<HomeViewModel>()
            Home(
                token = token,
                homeViewModel = homeViewModel,
                navController = navHostController

            )
        }
        composable(route = Screen.Localize.route) {
            Maps()
        }
        composable(route = Screen.Match.route) {
            val homeViewModel = hiltViewModel<HomeViewModel>()
            Match(
                token = token,
                homeViewModel = homeViewModel
            )
        }
        composable(route = Screen.Favorites.route) {
            Favorites()
        }
        composable(route = Screen.Message.route) {
            Message()
        }

        composable(
            route = "detail/{userId}",
        ) { backStackEntry ->
            val homeViewModel = hiltViewModel<HomeViewModel>()
            Log.d("Navigation en Screen", "Ha pasado")
            DetailBurger(
                string = backStackEntry.arguments?.getString("userId"),
                token = token,
                homeViewModel = homeViewModel,
            )
            Log.d("Navigation en Screen", "Ha Terminado")
        }

    }
}