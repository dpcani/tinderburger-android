package com.marbleinteractive.BiteBurger

import android.content.Context

class Prefs(val context: Context) {

    val SHARED_TOKEN = "token"
    val storage = context.getSharedPreferences("MyDb", 0)

    fun saveToken(token:String){
        storage.edit().putString(SHARED_TOKEN, token).apply()
    }

    fun getToken(): String? {
        return storage.getString(SHARED_TOKEN, "")
    }
}
