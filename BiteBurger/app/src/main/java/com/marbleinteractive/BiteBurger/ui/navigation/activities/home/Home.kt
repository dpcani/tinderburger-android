package com.marbleinteractive.BiteBurger.ui.navigation.activities


import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController

import com.marbleinteractive.BiteBurger.ui.components.CustomCellCard
import com.marbleinteractive.BiteBurger.ui.components.RowFilter
import com.marbleinteractive.BiteBurger.ui.components.ToggleFilterButtons
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.homeActivity.HeaderList
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.homeActivity.SearchBox
import com.marbleinteractive.BiteBurger.ui.navigation.activities.home.viewModel.HomeViewModel



@Composable
fun Home(
    token: String = "",
    homeViewModel: HomeViewModel = viewModel(),
    navController: NavHostController,
) {
    homeViewModel.listBurgers(tokenRequest = token)
    val listado by homeViewModel.listData.observeAsState()

    /** Properties **/
    val primaryColor = Color(0xFF7C7345)
    val secundaryColor = Color(0xFF2D2931)
    var filterString by remember { mutableStateOf("Elegir Filtro") }

    val gradient45 = Brush.linearGradient(
        colors = listOf(secundaryColor, secundaryColor, primaryColor),
        start = Offset(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY),
        end = Offset(0f, 0f),
        tileMode = TileMode.Clamp
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxSize()
            .background(
                gradient45
            )
    )
    {}
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {

        Column() {
            Spacer(modifier = Modifier.padding(vertical = 8.dp))
            HeaderList()
            SearchBox(
                hint = "Buscar Establecimiento",
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .padding(12.dp)
            )
            ToggleFilterButtons { filterString = it }
            Divider(
                Modifier
                    .fillMaxWidth(0.92f)
                    .padding(start = 12.dp)
            )
            RowFilter(filterString)
            Spacer(modifier = Modifier.padding(vertical = 8.dp))

            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                /** Listado **/
                LazyColumn(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.fillMaxWidth(0.95f)
                ) {
                    //TODO: Celda Personalizada
                    listado?.let { elementos ->
                        items(elementos.count()) { //<- Aqui el numero de elementos del listado
                            CustomCellCard(
                                elemento = elementos[it],
                                onClick = { myBurgerDetail ->
                                    navController.navigate("detail/${myBurgerDetail}") {
                                        popUpTo("home")
                                    }
                                })
                            Spacer(modifier = Modifier.padding(vertical = 4.dp))

                        }
                    }
                }
            }

        }
    }
}