package com.marbleinteractive.BiteBurger

import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels

import androidx.compose.material.Surface

import androidx.compose.ui.graphics.Color
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.installations.FirebaseInstallations

import com.marbleinteractive.BiteBurger.ui.login.viewModel.LoginViewModel

import com.marbleinteractive.BiteBurger.ui.navigation.NavigationScreen
import com.marbleinteractive.BiteBurger.ui.navigation.activities.Home
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    /** Properties **/
    private val viewModel: LoginViewModel by viewModels()

    /** LifeCicle **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("LifeCicle", "onCreate")
        setContent {
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }

        // Evento lanzado a analytics
        val analytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString("message", "Integración de Firebase completa")
        analytics.logEvent("InitLogin", bundle)

        notification()
    }

    override fun onStart() {
        super.onStart()
        Log.d("LifeCicle", "onStart")
        setContent {
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("LifeCicle", "onResume")
        setContent{
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d("LifeCicle", "onPause")
        setContent{
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }
    }

    override fun onStop() {
        super.onStop()
        Log.d("LifeCicle", "onStop")
        setContent{
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("LifeCicle", "onDestroy")
        setContent{
            Surface(
                color = Color.Black
            ) {
                NavigationScreen(viewModel = viewModel)

            }
        }
    }

    private fun notification() {
        FirebaseInstallations.getInstance().id.addOnCompleteListener {
            it.result?.let {
                println("Este es el token ${it}")
            }
        }
    }

}

