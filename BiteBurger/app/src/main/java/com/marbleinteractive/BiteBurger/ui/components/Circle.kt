package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.R
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.api.model.login.loginResponse.DataListResponse
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont


@Preview
@Composable
fun CirclePreview() {
    Circle(
        elementos = DataListResponse(
            id = 1,
            id_restaurant = 1,
            name = "ChessBurger",
            description = "kldsldsfjksklsfhhjfgjjdhddhgdfghfjhfjfjghfj",
            image = "",
            price = 15.0,
            time = 15,
            size = "xl",
            score = 8f,
            logo = "")
    )
}


@Composable
fun Circle(
    sizeDraw: Dp = 400.dp,
    elementos: DataListResponse
) {

    val circleColor = Color(0xAB211E24)
    val shapeCircle = CircleShape
    /** Fondo **/
    Box(
        modifier = Modifier
            .width(600.dp)
            .height(400.dp)
            .background(Color.Transparent)
            .offset(x = 0.dp),
    )
    {
        Box(
            modifier = Modifier
                .offset(x = 60.dp)
                .clip(shapeCircle)
                .background(circleColor)
                .size(sizeDraw),
        ) {
            Row(
                modifier = Modifier
                    .size(220.dp)
                    .align(Alignment.Center),
            ) {
                /**  Composicion de letras e imagenes principales**/
                Column(
                    verticalArrangement = Arrangement.Bottom
                ) {
                    Image(
                        painter = rememberAsyncImagePainter(model = "https://7uzmiokv.directus.app/assets/${elementos.logo}" ),
                        contentDescription = "Logo Burger",
                        modifier = Modifier
                            .padding(start = 8.dp)
                            .size(50.dp, 50.dp)
                            .offset(x= -8.dp,y = 16.dp),
                    )
                }
                Column(
                    modifier = Modifier
                        .width(2150.dp)
                ) {
                    Text(
                        color = Color.White,
                        text = elementos.name,
                        fontFamily = CustomFont,
                        fontSize = 24.sp,
                        modifier = Modifier.offset(y=-8.dp)
                    )
                    Text(
                        text = elementos.description.uppercase(),
                        color = Color.Gray,
                        fontFamily = CustomFont,
                        fontSize = 14.sp,
                        maxLines = 2,

                    )
                }
            }
        }
        /** Circulo principal para los demás elementos **/
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .offset(y = 96.dp, x = 0.dp),
        ) {
            Image(
                painter = rememberAsyncImagePainter(model = "https://7uzmiokv.directus.app/assets/${elementos.image}" ),
                contentDescription = "Likes Burger",
                modifier = Modifier
                    .size(300.dp, 300.dp)
            )
            Text(
                modifier = Modifier
                    .padding(top = 24.dp)
                    .offset(x = -16.dp,y = 58.dp ),
                text = "size: ".uppercase(),
                fontSize = 14.sp,
                color = Color.Gray
            )
            Text(
                modifier = Modifier
                    .padding(top = 24.dp)
                    .offset(x = -16.dp,y = 50.dp ),
                text = elementos.size.uppercase(),
                fontSize = 26.sp,
                color = Color.Gray,
                fontWeight = FontWeight.ExtraBold

            )
        }
    }
}