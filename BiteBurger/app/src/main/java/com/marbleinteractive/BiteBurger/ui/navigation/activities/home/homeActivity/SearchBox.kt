package com.marbleinteractive.BiteBurger.ui.navigation.activities.home.homeActivity

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Preview
@Composable
fun SearchBox(
    modifier: Modifier = Modifier,
    hint: String = "",
    onSearch: (String) -> Unit = {}
) {
    var text by remember { mutableStateOf("")}
    var isHintDisplay by remember { mutableStateOf(hint != "")}
    val circleShape = RoundedCornerShape(25)

    Box(
        modifier = modifier
    ) {
        BasicTextField(
            value = text,
            onValueChange = {
                text = it
                onSearch(it)
                isHintDisplay = false
            },
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = Color.Black,
                fontFamily = CustomFont),
            modifier = Modifier
                .fillMaxWidth()
                .shadow(5.dp, circleShape)
                .background(Color.White, circleShape)
                .padding(
                    horizontal = 40.dp,
                    vertical = 12.dp,
                )
                .onFocusChanged {},

        )
        if (isHintDisplay) {
            Text(
                text = hint,
                color = Color.LightGray,
                modifier = Modifier
                    .padding(horizontal = 40.dp, vertical = 12.dp,),
                textAlign = TextAlign.Start,

            )
        }
        IconButton(
            modifier = Modifier
                .alpha(ContentAlpha.medium),
            onClick = {}
        ) {
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = "Search Icon",
                tint = Color.Black,)
        }

    }

}