package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Preview
@Composable
fun RowFilter(filter: String = "Defecto") {

    val color = Color(0xFFFF6464)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start,
        modifier = Modifier.padding(start = 16.dp)
    ) {
        Text(text ="Burgers:", fontSize = 18.sp, color = Color.White )
        Text(text = filter, fontSize = 20.sp, color = color, modifier = Modifier.padding(start = 8.dp) )
    }
}