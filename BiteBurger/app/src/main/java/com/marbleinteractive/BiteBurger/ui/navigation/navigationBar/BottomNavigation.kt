package com.marbleinteractive.BiteBurger.ui.navigation.navigationBar

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.compose.rememberNavController
import com.marbleinteractive.BiteBurger.ui.navigation.CustomBottomNavigation
import com.marbleinteractive.BiteBurger.ui.utils.navigation.BottomNavHost

@Composable
fun BottomNavigation(
    token: String
) {

    /** Properties **/
    val navController = rememberNavController()

    /** Components **/
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Transparent
    ) {
        Scaffold(
            bottomBar = {
                CustomBottomNavigation(
                    navController = navController,
                    items = NavProps.listItems
                )
            }
        ) {
            BottomNavHost(
                navHostController = navController,
                token = token
            )
        }
    }

}