package com.marbleinteractive.BiteBurger.ui.utils.navigation

import androidx.annotation.StringRes
import com.marbleinteractive.BiteBurger.R

sealed class Screen (
    val route: String,
    @StringRes val title: Int,
    val icon:Int)
{
    object Home: Screen("home", R.string.home, R.drawable.ic_one)
    object Localize: Screen("localize", R.string.map, R.drawable.ic_two)
    object Match: Screen("match", R.string.match, R.drawable.ic_center)
    object Favorites: Screen("favorites", R.string.favorites, R.drawable.ic_four)
    object Message: Screen("message", R.string.message, R.drawable.ic_five)


}
