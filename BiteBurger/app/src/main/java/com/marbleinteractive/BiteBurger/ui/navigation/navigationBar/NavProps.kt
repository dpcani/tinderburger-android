package com.marbleinteractive.BiteBurger.ui.navigation.navigationBar

import com.marbleinteractive.BiteBurger.ui.utils.navigation.Screen

object NavProps {
    val listItems = listOf(Screen.Home, Screen.Localize, Screen.Match, Screen.Favorites, Screen.Message)
}