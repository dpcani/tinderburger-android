package com.marbleinteractive.BiteBurger.ui.components

import android.graphics.drawable.shapes.Shape
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.marbleinteractive.BiteBurger.ui.theme.CustomFont

@Composable
fun CustomButtonFilter(
    stringButton:String = "Vegana",
    state:Boolean = false,
    clickListener:(Int) -> Unit,
    index:Int)
{
    val primaryColor = Color.Transparent
    val secundaryColor = Color(0xFFF44336)
    val paddingValue = 16.dp

    Text(
        text = stringButton,
        fontFamily = CustomFont,
        color = Color.White,
        modifier = Modifier
                .padding(top = paddingValue/2, end = paddingValue, bottom = paddingValue/2)
                .clickable {
                    clickListener(index)
                }
                .background(
                    if (state) {
                        secundaryColor
                    } else {
                        primaryColor
                    }
                ))

}