package com.example.api.model.login.loginResponse

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("data") var loginData: LoginDataResponse,
)
