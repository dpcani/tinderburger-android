package com.marbleinteractive.BiteBurger.ui.navigation.activities.burgerLoading

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import com.airbnb.lottie.compose.*
import com.marbleinteractive.BiteBurger.R

@Preview
@Composable
fun BurgerLoading()
{
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.burger_loading))
    val progress by animateLottieCompositionAsState(composition)

    Column(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .background(Color.Transparent),
        verticalArrangement = Arrangement.Center
    ) {

        LottieAnimation(composition, progress)
    }
}




