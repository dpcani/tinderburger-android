package com.marbleinteractive.BiteBurger.ui.navigation

sealed class Destination (val route: String) {
    object Login: Destination(route = "login")
    object Detail: Destination(route = "detail")
    object Home: Destination(route = "home")


    companion object {
        fun getStartDestination() = Login.route
    }
}