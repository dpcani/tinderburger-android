package com.marbleinteractive.BiteBurger.ui.navigation.activities.home.viewModel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.api.model.login.loginResponse.DataListResponse
import com.example.api.model.login.loginResponse.ListResponse
import com.example.api.model.login.loginResponse.LoginResponse
import com.marbleinteractive.BiteBurger.network.repository.RetrofitHelper
import com.marbleinteractive.BiteBurger.network.request.DataListRequest
import com.marbleinteractive.BiteBurger.network.request.LoginRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class HomeViewModel @Inject constructor() : ViewModel() {

    val isSuccessLoading = mutableStateOf(value = false)
    val listData = MutableLiveData<List<DataListResponse>>()

    fun listBurgers(tokenRequest: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val authService = RetrofitHelper.getAuthService()
            authService.getListBurgers(token = "Bearer ${tokenRequest}")
                .enqueue(object : Callback<ListResponse> {
                    override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                        Log.d("List Burgers", "Fallo Response")
                    }
                    override fun onResponse(
                        call: Call<ListResponse>,
                        response: Response<ListResponse>,
                    ) {
                        Log.d("List Burgers", "Acierto Response.. Comprobando..")
                        if (response.isSuccessful) {
                            isSuccessLoading.value = true
                            response.body()?.let { datos ->
                                Log.d("List Burgers", "Respuesta del Response Correcto: $datos")
                                listData.value = datos.data
                            }
                        } else {
                            isSuccessLoading.value = false
                            response.errorBody()?.let { error ->
                                error.close()
                                Log.d("List Burgers", "Respuesta Response Error: $error")

                            }
                        }

                    }
                })
        }
    }
}

