package com.example.api.model.login.loginResponse

import com.google.gson.annotations.SerializedName

data class ListResponse(
    @SerializedName("data") var data: List<DataListResponse>,
)
