package com.marbleinteractive.BiteBurger.ui.login

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.*
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.widget.ContentLoadingProgressBar
import com.davidorellana.logincomposeretrofit2.ui.login.components.ProgressBarLoading
import com.marbleinteractive.BiteBurger.R
import com.marbleinteractive.BiteBurger.ui.components.CustomButtomSegments
import com.marbleinteractive.BiteBurger.ui.components.CustomButtonLogin
import com.marbleinteractive.BiteBurger.ui.components.CustomTextField
import com.marbleinteractive.BiteBurger.ui.components.CustomTextFieldPassword
import com.marbleinteractive.BiteBurger.ui.singUp.SingUpActivity

@Composable
fun LoginScreen(
    modifier: Modifier = Modifier,
    loadingProgressBar: Boolean,
    onClickLogin: (email: String, password: String) -> Unit,
    errorCheck: Boolean,
) {

    /** Properties **/
    var stateScreen by remember { mutableStateOf(true) }

    var email by rememberSaveable { mutableStateOf(value = "prueba@practica.es") }
    var password by rememberSaveable { mutableStateOf(value = "123Practica@") }

    var name by rememberSaveable { mutableStateOf(value = "") }
    var surname by rememberSaveable { mutableStateOf(value = "") }

    val isValidate by derivedStateOf { email.isNotBlank() && password.isNotBlank() }
    val focusManager = LocalFocusManager.current

    /** Components **/
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF1A171D))
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_imagen),
            contentDescription = "Imagen Fondo",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .height(400.dp) //<- Modificar tamaño
        )
        Image(
            painter = painterResource(id = R.drawable.ic_logoburger),
            contentDescription = "Logo",
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .height(100.dp)
                .align(Alignment.TopEnd)
                .alpha(0.8f)
                .padding(top = 16.dp)
        )
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.BottomCenter
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(550.dp)
                    .background(Color.Transparent),

                shape = RoundedCornerShape(
                    topStartPercent = 8,
                    topEndPercent = 8
                )
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth()
                        .background(
                            brush = Brush.verticalGradient(
                                colors = listOf(
                                    Color(0xFF26222A),
                                    Color(0xFF000000)
                                )
                            )
                        ),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.spacedBy(2.dp)
                ) {

                    stateScreen = CustomButtomSegments()
                    when (stateScreen) {
                        false -> {
                            CustomTextField(
                                captionString = "INTRODUCE TU USUARIO",
                                textValue = email,
                                onValueChange = { email = it },
                                onClickButton = { email = ""  },
                                onNext = { focusManager.moveFocus(FocusDirection.Down) },
                                condicion = false
                            )
                            Spacer(modifier = Modifier.padding(4.dp))
                            CustomTextFieldPassword(
                                captionString = "INTRODUCE LA CLAVE",
                                textValue = password,
                                onValueChange = { password = it },
                                onDone = { focusManager.clearFocus() }
                            )
                            if (errorCheck) {
                                Text(
                                    text = "Error de validación. Usuario o password erroneos",
                                    color = Color.Red)
                            }
                            Spacer(modifier = Modifier.padding(62.dp))
                            CustomButtonLogin(
                                stringButton = "Entrar",
                                onClick = { onClickLogin(email, password) },
                                enable = isValidate
                            )
                        }
                        true -> {
                            LazyColumn(
                                horizontalAlignment = Alignment.CenterHorizontally,
                            ) {
                                item {
                                    CustomTextField(
                                        captionString = "NOMBRE ",
                                        textValue = name,
                                        onValueChange = { name = it },
                                        onClickButton = {},
                                        onNext = { focusManager.moveFocus(FocusDirection.Down) },
                                        condicion = false)
                                    Spacer(modifier = Modifier.padding(4.dp))
                                }
                                item {
                                    CustomTextField(
                                        captionString = "APELLIDOS ",
                                        textValue = surname,
                                        onValueChange = { surname = it },
                                        onClickButton = {},
                                        onNext = { focusManager.moveFocus(FocusDirection.Down) },
                                        condicion = false)
                                    Spacer(modifier = Modifier.padding(4.dp))
                                }
                                item {
                                    CustomTextField(
                                        captionString = "EMAIL ",
                                        textValue = email,
                                        onValueChange = { email = it },
                                        onClickButton = {},
                                        onNext = { focusManager.moveFocus(FocusDirection.Down) },
                                        condicion = false)
                                    Spacer(modifier = Modifier.padding(4.dp))
                                }
                                item {
                                    CustomTextField(
                                        captionString = "CONTRASEÑA ",
                                        textValue = password,
                                        onValueChange = { password = it },
                                        onClickButton = {},
                                        onNext = { focusManager.clearFocus() },
                                        condicion = false)
                                    Spacer(modifier = Modifier.padding(2.dp))
                                }
                                item {
                                    SingUpActivity()
                                    Spacer(modifier = Modifier.padding(8.dp))
                                    CustomButtonLogin( //<- configurar para el regisrar usuario
                                        stringButton = "Registrar",
                                        onClick = { },
                                        enable = false)
                                    Spacer(modifier = Modifier.padding(16.dp))
                                }
                            }
                        }
                    }
                    ProgressBarLoading(isLoading = loadingProgressBar)
                }
            }
        }
    }
}

