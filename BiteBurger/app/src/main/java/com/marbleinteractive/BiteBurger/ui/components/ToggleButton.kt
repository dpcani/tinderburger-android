package com.marbleinteractive.BiteBurger.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import java.util.*

@Composable
fun ToggleFilterButtons(currentSelection:(String)->Unit) {

    var selectedOption by rememberSaveable { mutableStateOf(-1) }
    val filter = listOf("Angus", "Cerdo", "Pollo", "Vegana", "Pulled", "Vaca Vieja", "Costillar")

    LazyRow(
        horizontalArrangement = Arrangement.Start,
        modifier = Modifier
            .padding(start = 16.dp)
    ) {
        itemsIndexed(filter) { index, item ->
            CustomButtonFilter( item, index==selectedOption, {
                selectedOption=it
                currentSelection(item)}, index)
        }
    }
}

